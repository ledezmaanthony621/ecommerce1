package com.example.ecommerce1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
public class Ecommerce1Application {

	public static void main(String[] args) {
		SpringApplication.run(Ecommerce1Application.class, args);
	}

}
